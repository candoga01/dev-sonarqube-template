def getFolderName() {
  def array = pwd().split("/")
  return array[array.length - 2];
}
pipeline {
  agent any
  environment {
    BRANCHES = "${env.GIT_BRANCH}"
    COMMIT = "${env.GIT_COMMIT}"
    RELEASE_NAME = "sonarqube"
    SERVICE_PORT = "${APP_PORT}"
    DOCKERHOST = "${DOCKERHOST_IP}"
    ACTION = "${ACTION}"
    DEPLOYMENT_TYPE = "${DEPLOYMENT_TYPE == ""? "EC2":DEPLOYMENT_TYPE}"
    KUBE_SECRET = "${KUBE_SECRET}"
    foldername = getFolderName()

  }
  stages {
    stage('init') {
      steps {
        script {

          def job_name = "$env.JOB_NAME"
          print(job_name)
          def values = job_name.split('/')
          namespace_prefix = values[0].replaceAll("[^a-zA-Z0-9]+","").toLowerCase().take(50)
          namespace = "$namespace_prefix-$env.foldername".toLowerCase()
          service = values[2].replaceAll("[^a-zA-Z0-9]+","").toLowerCase().take(50)
          print("kube namespace: $namespace")
          print("service name: $service")
          env.namespace_name=namespace
          env.service=service
        }
      }
    }

    stage('Deploy') {

      steps {
        script {
          echo "echoed folder--- $foldername"

          if (env.DEPLOYMENT_TYPE == 'EC2') {

            sh 'ssh -o "StrictHostKeyChecking=no" ciuser@$DOCKERHOST "docker volume create --name sonarqube_data"'
            sh 'ssh -o "StrictHostKeyChecking=no" ciuser@$DOCKERHOST "docker volume create --name sonarqube_logs"'
            sh 'ssh -o "StrictHostKeyChecking=no" ciuser@$DOCKERHOST "docker volume create --name sonarqube_extensions"'
            
            sh 'ssh -o "StrictHostKeyChecking=no" ciuser@$DOCKERHOST "docker run -d --name sonarqube -p 9001:9000  -v sonarqube_conf:/opt/sonarqube/conf   -v sonarqube_extensions:/opt/sonarqube/extensions   -v sonarqube_logs:/opt/sonarqube/logs  -v sonarqube_data:/opt/sonarqube/data sonarqube:latest"'

          }
          if (env.DEPLOYMENT_TYPE == 'KUBERNETES') {
                          withCredentials([file(credentialsId: "$KUBE_SECRET", variable: 'KUBECONFIG')]) {
                  sh '''

                    rm -rf kube
                    mkdir -p kube
                    cp "$KUBECONFIG" kube
                    kubectl create ns "$namespace_name" || true
                    kubectl apply -f ./charts -n "$namespace_name"
                    sleep 30
                    kubectl get svc -n "$namespace_name"
                  '''
                  script {
                    env.temp_service_name = "$RELEASE_NAME".take(63)
                    def url = sh (returnStdout: true, script: '''kubectl get svc -n "$namespace_name" | grep "$temp_service_name" | awk '{print $4}' ''').trim()
                    if (url != "<pending>") {
                      print("##\$@\$ http://$url ##\$@\$")
                    }
                  }
            }
          }
        }
      }
    }

    stage('Destroy') {
      when {
        expression {
          env.DEPLOYMENT_TYPE == 'EC2' && env.ACTION == 'DESTROY'
        }
      }
      steps {
        script {
          if (env.DEPLOYMENT_TYPE == 'EC2') {
            sh 'ssh -o "StrictHostKeyChecking=no" ciuser@$DOCKERHOST "docker stop sonarqube"'
          }
          if (env.DEPLOYMENT_TYPE == 'KUBERNETES') {
              withCredentials([file(credentialsId: "$KUBE_SECRET", variable: 'KUBECONFIG')]) {
                  sh '''
                    kubectl delete -f ./charts -n "$namespace_name"
                  '''
            }
          }
        }
      }
    }
  }
}
